<?php

namespace App;

class Game
{
    private $id;

    private $player1;
    private $player2;

    private $logger;

    public $grid = [
        [null, null, null],
        [null, null, null],
        [null, null, null],
    ];

    /**
     * Constructor
     */
    public function __construct($player1, $player2, $logger)
    {
        $this->id = bin2hex(random_bytes(16));

        $this->player1 = $player1;
        $this->player2 = $player2;

        $this->logger = $logger;
    }

    public function getId()
    {
        return $this->id;
    }

    public function getGrid()
    {
        return $this->grid;
    }

    public function start()
    {
        $winScore = 1;
        $tieScore = 0;
        $loseScore = -1;

        while (true) {
            // $this->logger->writeLn('Player 1 PLAY');
            $this->player1->play($this);
            if ($this->hasWinner()) {
                $this->logger->writeLn('Player 1 WIN');
                $this->player1->endGame($this, $winScore);
                $this->player2->endGame($this, $loseScore);
                break;
            }
            if ($this->isOver()) {
                $this->logger->writeLn('TIE');
                $this->player1->endGame($this, $tieScore);
                $this->player2->endGame($this, $tieScore);
                break;
            }

            // $this->logger->writeLn('Player 2 PLAY');
            $this->player2->play($this);
            if ($this->hasWinner()) {
                $this->logger->writeLn('Player 2 WIN');
                $this->player1->endGame($this, $loseScore);
                $this->player2->endGame($this, $winScore);
                break;
            }
            if ($this->isOver()) {
                $this->logger->writeLn('TIE');
                $this->player1->endGame($this, $tieScore);
                $this->player2->endGame($this, $tieScore);
                break;
            }
        }
    }

    public function hasWinner()
    {
        foreach ($this->grid as $row) {
            if (
                $row[0] !== null
                && $row[1] !== null
                && $row[2] !== null
                && $row[0] == $row[1]
                && $row[1] == $row[2]
            ) {
                return true;
            }
        }

        for ($i=0; $i < 3; $i++) {
            if (
                $this->grid[0][$i] !== null
                && $this->grid[1][$i] !== null
                && $this->grid[2][$i] !== null
                && $this->grid[0][$i] == $this->grid[1][$i]
                && $this->grid[1][$i] == $this->grid[2][$i]
            ) {
                return true;
            }
        }

        if (
            $this->grid[0][0] !== null
            && $this->grid[0][0] == $this->grid[1][1]
            && $this->grid[1][1] == $this->grid[2][2]
        ) {
            return true;
        }

        if (
            $this->grid[0][2] !== null
            && $this->grid[0][2] == $this->grid[1][1]
            && $this->grid[1][1] == $this->grid[2][0]
        ) {
            return true;
        }

        return false;
    }

    public function isOver()
    {
        foreach ($this->grid as $row) {
            foreach ($row as $col) {
                if ($col === null) {
                    return false;
                }
            }
        }
        return true;
    }

    public function move(Player $player, $move)
    {
        $i = 0;
        foreach ($this->grid as &$row) {
            foreach ($row as &$col) {
                if ($i == $move) {
                    $col = $player->getSymbol();
                    break 2;
                }
                $i++;
            }
        }

        // $this->logger->writeLn(json_encode($this->grid));
    }
}
