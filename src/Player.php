<?php

namespace App;

class Player
{
    private $symbol;
    private $strategy;

    /**
     * Constructor
     */
    public function __construct(string $symbol)
    {
        $this->symbol = $symbol;
        $this->strategy = new PlayerStrategy($this);
    }

    /**
     * Get symbol
     */
    public function getSymbol()
    {
        return $this->symbol;
    }

    /**
     * Play the game !
     */
    public function play(Game $game)
    {
        $bestMoves = $this->strategy->getBestMoves($game);

        $move = null;

        foreach ($bestMoves as $bestMove => $score) {
            if ($score > 20) {
                $move = $bestMove;
                break;
            }
        }

        if ($move === null) { //  or random_int(1, 10) <= 3
            $possiblesMoves = $this->findPossiblesMoves($game);

            shuffle($possiblesMoves);
            $move = $possiblesMoves[0];
        }

        $this->strategy->learn($game, $move);

        $game->move($this, $move);
    }

    /**
     * Find possibles moves
     */
    private function findPossiblesMoves($game)
    {
        $grid = $game->getGrid();
        $possiblesMoves = [];
        $i = 0;

        foreach ($grid as $row) {
            foreach ($row as $col) {
                if ($col === null) {
                    $possiblesMoves[] = $i;
                }
                $i++;
            }
        }

        return $possiblesMoves;
    }

    /**
     * The game is ended
     */
    public function endGame(Game $game, $score)
    {
        $this->strategy->setScore($game, $score);
    }
}
