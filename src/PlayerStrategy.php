<?php

namespace App;

class PlayerStrategy
{
    private $player;
    private $memory = [];

    /**
     * Constructor
     */
    public function __construct(Player $player)
    {
        $this->player = $player;
    }

    /**
     * Learn at eatch moves
     */
    public function learn(Game $game, $move)
    {
        $this->memory[] = [
            'game_id' => $game->getId(),
            'grid_hash' => sha1(json_encode($game->getGrid())),
            'move' => $move,
            'score' => 0,
        ];

        // print_r($this->memory);
    }

    /**
     * Set the scrore after the is over
     */
    public function setScore($game, $score)
    {
        foreach ($this->memory as &$row) {
            if ($row['game_id'] == $game->getId()) {
                $row['score'] = $score;
            }
        }
    }

    /**
     * Try to get the best move
     */
    public function getBestMoves($game)
    {
        $gridHash = sha1(json_encode($game->getGrid()));
        $result = [];
        foreach ($this->memory as $row) {
            if ($gridHash == $row['grid_hash']) {
                if (!isset($result[$row['move']])) {
                    $result[$row['move']] = 0;
                }
                $result[$row['move']] += $row['score'];
            }
        }

        arsort($result);

        return $result;
    }
}
