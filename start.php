<?php

require __DIR__.'/vendor/autoload.php';

use App\Game;
use App\Player;
use App\Logger;

$player1 = new Player('X');
$player2 = new Player('O');

while (true) {
    $game = new Game($player1, $player2, new Logger());
    $game->start();
}
